#!/bin/bash

OS_NAME=$(. /etc/os-release; echo $NAME)
OS_VERSION=$(. /etc/os-release; echo $VERSION)

echo "Provisioning $(hostname)...."

if [[ "$OS_NAME" =~ Red\ Hat* ]] || [[ "$OS_NAME" =~ CentOS* ]]; then


  	if [ "$OS_NAME" = "CentOS Linux" ]; then
	  	if [ "$OS_VERSION" = "8" ]; then
		  	dnf -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
		elif  [ "$OS_VERSION" = "9" ]; then
			dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm	  			
		fi  	
	fi

  	if [ "$OS_NAME" = "CentOS Stream" ]; then
	  	if [ "$OS_VERSION" = "8" ]; then
		  	dnf config-manager --set-enabled powertools
		elif  [ "$OS_VERSION" = "9" ]; then
			dnf config-manager --set-enabled crb	  			
		fi  	
		dnf install epel-release epel-next-release	
	fi

  	if [ "$OS_NAME" = "Red Hat Enterprise Linux" ]; then
  	  	if [ "$OS_VERSION" = "8" ]; then
		  	subscription-manager repos --enable codeready-builder-for-rhel-8-$(arch)-rpms
			dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
		elif  [ "$OS_VERSION" = "9" ]; then
			subscription-manager repos --enable codeready-builder-for-rhel-9-$(arch)-rpms
			dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm	  			
		fi 
  	
		for repo in 						\
			rhel-8-for-x86_64-supplementary-rpms		\
			ansible-2-for-rhel-8-x86_64-rpms		\
			rhel-atomic-7-cdk-3.11-rpms \
		; do
		
	    		subscription-manager repos --enable=${repo}
		
		done;
	fi
	
	dnf -y update
	
  	if [ "$OS_VERSION" = "8" ]; then
	  	dnf -y install @python36
	elif  [ "$OS_VERSION" = "9" ]; then
	  	dnf -y install python
	fi 	
	
	dnf -y install acpid ansible unzip git curl python3-policycoreutils python3-dns python3-psycopg2 python3-requests python3-netaddr
elif [[ "$OS_NAME" = "Ubuntu" ]]; then
	apt update -y 
	apt upgrade -y 
	apt install -y software-properties-common
	apt-add-repository --yes --update ppa:ansible/ansible
	apt install -y aptitude curl unzip git ansible
elif [[ "$OS_NAME" = "Amazon Linux" ]]; then
	yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
	yum install -y amazon-linux-extras
	amazon-linux-extras enable java-openjdk11 docker postgresql11 golang1.11
	yum -y install unzip python-passlib policycoreutils-python python2-pip python-dns python-psycopg2 python-requests python-netaddr ansible git curl
fi

curl -o /etc/ansible/requirements.yml https://gitlab.com/alessandro.dibella/provisioning/ansilble/playbooks/raw/master/requirements.yml
curl -o /etc/ansible/localhost.yaml https://gitlab.com/alessandro.dibella/provisioning/ansilble/playbooks/raw/master/localhost.template.yml
ansible-galaxy install -r /etc/ansible/requirements.yml

echo -e "\n\n\n"
echo -e "*************************************************************************************************************************************************"
echo -e "*\tReview \033[0;31m/etc/ansible/localhost.yaml\033[0m and run \033[0;31mansible-playbook --become --connection=local /etc/ansible/localhost.yaml\033[0m to provision the box\t\t*"
echo -e "*************************************************************************************************************************************************"
